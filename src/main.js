import Vue from 'nativescript-vue';

import router from './router';

import store from './store';

import './styles.scss';

import firebase from 'nativescript-plugin-firebase'

firebase.init({
  // Optionally pass in properties for database, authentication and cloud messaging,
  // see their respective docs.
}).then(
    function (instance) {
       console.log(instance)
      console.log("firebase.init done");

    },
    function (error) {
      console.log("firebase.init error: " + error);
    }
);

// Uncomment the following to see NativeScript-Vue output logs
//Vue.config.silent = false;

new Vue({

  router,

  store,

}).$start();
